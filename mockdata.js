module.exports = {
  products: [
    {
      id: 1,
      name: "Ice - Clear, 300 Lb For Carving",
      price: "€16.75",
      picture: "http://dummyimage.com/616x737.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 2,
      name: "Tomatoes",
      price: "€17.33",
      picture: "http://dummyimage.com/477x687.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 3,
      name: "Ice Cream - Turtles Stick Bar",
      price: "€85.92",
      picture: "http://dummyimage.com/748x652.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 4,
      name: "Tia Maria",
      price: "€89.87",
      picture: "http://dummyimage.com/769x466.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 5,
      name: "Cups 10oz Trans",
      price: "€93.51",
      picture: "http://dummyimage.com/789x729.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 6,
      name: "Cinnamon Buns Sticky",
      price: "€96.52",
      picture: "http://dummyimage.com/727x693.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 7,
      name: "Lemonade - Strawberry, 591 Ml",
      price: "€72.17",
      picture: "http://dummyimage.com/593x776.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 8,
      name: "Muffin Hinge - 211n",
      price: "€18.51",
      picture: "http://dummyimage.com/456x581.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 9,
      name: "Wine - Maipo Valle Cabernet",
      price: "€41.46",
      picture: "http://dummyimage.com/438x653.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 10,
      name: "Syrup - Monin, Amaretta",
      price: "€4.90",
      picture: "http://dummyimage.com/662x631.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 11,
      name: "Bread - Raisin Walnut Pull",
      price: "€62.13",
      picture: "http://dummyimage.com/676x655.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 12,
      name: "Cheese - St. Andre",
      price: "€14.83",
      picture: "http://dummyimage.com/447x688.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 13,
      name: "Oneshot Automatic Soap System",
      price: "€52.82",
      picture: "http://dummyimage.com/423x685.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 14,
      name: "Wine - Delicato Merlot",
      price: "€91.93",
      picture: "http://dummyimage.com/408x478.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 15,
      name: "Bar Mix - Lime",
      price: "€39.99",
      picture: "http://dummyimage.com/593x426.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 16,
      name: "Clam - Cherrystone",
      price: "€86.09",
      picture: "http://dummyimage.com/607x637.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 17,
      name: "Heavy Duty Dust Pan",
      price: "€85.91",
      picture: "http://dummyimage.com/411x496.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 18,
      name: "Bread - Ciabatta Buns",
      price: "€47.77",
      picture: "http://dummyimage.com/773x767.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 19,
      name: "Wine - White, Antinore Orvieto",
      price: "€2.49",
      picture: "http://dummyimage.com/703x405.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 20,
      name: "Crab - Soft Shell",
      price: "€79.30",
      picture: "http://dummyimage.com/497x728.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 21,
      name: "Napkin Colour",
      price: "€51.76",
      picture: "http://dummyimage.com/790x689.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 22,
      name: "V8 Pet",
      price: "€0.99",
      picture: "http://dummyimage.com/627x626.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 23,
      name: "Water - Green Tea Refresher",
      price: "€2.85",
      picture: "http://dummyimage.com/689x513.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 24,
      name: "Browning Caramel Glace",
      price: "€77.53",
      picture: "http://dummyimage.com/551x597.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 25,
      name: "Muffin - Mix - Strawberry Rhubarb",
      price: "€95.68",
      picture: "http://dummyimage.com/710x500.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 26,
      name: "Foil Cont Round",
      price: "€14.90",
      picture: "http://dummyimage.com/731x421.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 27,
      name: "Vodka - Lemon, Absolut",
      price: "€50.06",
      picture: "http://dummyimage.com/699x796.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 28,
      name: "Yoghurt Tubes",
      price: "€35.69",
      picture: "http://dummyimage.com/404x426.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 29,
      name: "Fondant - Icing",
      price: "€9.57",
      picture: "http://dummyimage.com/586x697.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 30,
      name: "Chocolate - Semi Sweet",
      price: "€86.30",
      picture: "http://dummyimage.com/543x579.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 31,
      name: "Apple - Delicious, Golden",
      price: "€27.96",
      picture: "http://dummyimage.com/559x792.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 32,
      name: "Beer - Upper Canada Lager",
      price: "€31.12",
      picture: "http://dummyimage.com/531x446.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 33,
      name: "Wine - Magnotta, Merlot Sr Vqa",
      price: "€46.34",
      picture: "http://dummyimage.com/745x775.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 34,
      name: "Buffalo - Striploin",
      price: "€7.38",
      picture: "http://dummyimage.com/542x400.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 35,
      name: "Bread - Bistro White",
      price: "€62.18",
      picture: "http://dummyimage.com/608x470.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 36,
      name: "Pork - Kidney",
      price: "€69.62",
      picture: "http://dummyimage.com/643x430.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 37,
      name: "Pear - Prickly",
      price: "€41.59",
      picture: "http://dummyimage.com/450x554.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 38,
      name: "Vinegar - Champagne",
      price: "€25.43",
      picture: "http://dummyimage.com/759x619.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 39,
      name: "Vacuum Bags 12x16",
      price: "€60.19",
      picture: "http://dummyimage.com/578x421.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 40,
      name: "Pork - Smoked Kassler",
      price: "€57.23",
      picture: "http://dummyimage.com/524x704.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 41,
      name: "Urban Zen Drinks",
      price: "€56.50",
      picture: "http://dummyimage.com/447x757.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 42,
      name: "Corn - Cream, Canned",
      price: "€2.11",
      picture: "http://dummyimage.com/629x790.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 43,
      name: "Capicola - Hot",
      price: "€21.35",
      picture: "http://dummyimage.com/401x790.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 44,
      name: "Wine - Ruffino Chianti Classico",
      price: "€75.35",
      picture: "http://dummyimage.com/557x579.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 45,
      name: "Puree - Kiwi",
      price: "€10.21",
      picture: "http://dummyimage.com/537x659.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 46,
      name: "Cup - Paper 10oz 92959",
      price: "€26.86",
      picture: "http://dummyimage.com/624x420.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 47,
      name: "Sprouts - Corn",
      price: "€72.58",
      picture: "http://dummyimage.com/751x753.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 48,
      name: "Peppercorns - Green",
      price: "€11.11",
      picture: "http://dummyimage.com/676x759.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 49,
      name: "Bread - Corn Muffaletta",
      price: "€74.11",
      picture: "http://dummyimage.com/437x763.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 50,
      name: "Tart - Lemon",
      price: "€87.04",
      picture: "http://dummyimage.com/631x620.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 51,
      name: "Chicken Breast Halal",
      price: "€75.62",
      picture: "http://dummyimage.com/461x761.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 52,
      name: "Scallops - 20/30",
      price: "€7.48",
      picture: "http://dummyimage.com/729x467.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 53,
      name: "Smoked Tongue",
      price: "€50.63",
      picture: "http://dummyimage.com/557x550.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 54,
      name: "Seedlings - Buckwheat, Organic",
      price: "€35.40",
      picture: "http://dummyimage.com/767x576.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 55,
      name: "Mint - Fresh",
      price: "€99.48",
      picture: "http://dummyimage.com/517x800.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 56,
      name: "Sardines",
      price: "€13.42",
      picture: "http://dummyimage.com/751x714.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 57,
      name: "Steel Wool S.o.s",
      price: "€68.69",
      picture: "http://dummyimage.com/636x511.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 58,
      name: "Cookies Cereal Nut",
      price: "€83.36",
      picture: "http://dummyimage.com/693x665.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 59,
      name: "Roe - Lump Fish, Black",
      price: "€40.01",
      picture: "http://dummyimage.com/752x501.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 60,
      name: "Wine - Barolo Fontanafredda",
      price: "€48.41",
      picture: "http://dummyimage.com/743x634.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 61,
      name: "Ice Cream - Fudge Bars",
      price: "€79.42",
      picture: "http://dummyimage.com/448x683.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 62,
      name: "Apple - Northern Spy",
      price: "€57.82",
      picture: "http://dummyimage.com/546x780.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 63,
      name: "Beans - Kidney, Red Dry",
      price: "€25.72",
      picture: "http://dummyimage.com/682x573.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 64,
      name: "Shrimp, Dried, Small / Lb",
      price: "€54.68",
      picture: "http://dummyimage.com/612x518.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 65,
      name: "Bay Leaf",
      price: "€1.41",
      picture: "http://dummyimage.com/495x724.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 66,
      name: "Appetizer - Asian Shrimp Roll",
      price: "€84.71",
      picture: "http://dummyimage.com/541x497.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 67,
      name: "Shrimp - 21/25, Peel And Deviened",
      price: "€34.82",
      picture: "http://dummyimage.com/779x518.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 68,
      name: "Container - Hngd Cll Blk 7x7x3",
      price: "€12.96",
      picture: "http://dummyimage.com/557x589.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 69,
      name: "Wine - White, Ej",
      price: "€52.16",
      picture: "http://dummyimage.com/499x631.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 70,
      name: "Beef - Striploin",
      price: "€93.69",
      picture: "http://dummyimage.com/691x426.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 71,
      name: "Pasta - Rotini, Colour, Dry",
      price: "€90.38",
      picture: "http://dummyimage.com/650x579.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 72,
      name: "Nut - Hazelnut, Whole",
      price: "€35.35",
      picture: "http://dummyimage.com/519x417.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 73,
      name: "Mace",
      price: "€10.60",
      picture: "http://dummyimage.com/650x560.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 74,
      name: "Rice - Wild",
      price: "€20.77",
      picture: "http://dummyimage.com/774x728.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 75,
      name: "Beer - Fruli",
      price: "€77.03",
      picture: "http://dummyimage.com/731x769.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 76,
      name: "Coffee - 10oz Cup 92961",
      price: "€48.89",
      picture: "http://dummyimage.com/793x633.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 77,
      name: "Pop - Club Soda Can",
      price: "€21.88",
      picture: "http://dummyimage.com/613x654.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 78,
      name: "Cinnamon Rolls",
      price: "€74.72",
      picture: "http://dummyimage.com/568x686.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 79,
      name: "Flour - Whole Wheat",
      price: "€46.82",
      picture: "http://dummyimage.com/777x442.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 80,
      name: "Crush - Cream Soda",
      price: "€95.21",
      picture: "http://dummyimage.com/665x540.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 81,
      name: "Taro Leaves",
      price: "€13.03",
      picture: "http://dummyimage.com/767x583.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 82,
      name: "Truffle Cups Green",
      price: "€10.49",
      picture: "http://dummyimage.com/471x565.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 83,
      name: "Mushroom - Enoki, Fresh",
      price: "€41.86",
      picture: "http://dummyimage.com/737x731.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 84,
      name: "Onions - Red",
      price: "€37.47",
      picture: "http://dummyimage.com/451x582.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 85,
      name: "Bread - Raisin Walnut Pull",
      price: "€32.87",
      picture: "http://dummyimage.com/726x780.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 86,
      name: "Tea - Herbal Sweet Dreams",
      price: "€1.03",
      picture: "http://dummyimage.com/677x443.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 87,
      name: "Crush - Grape, 355 Ml",
      price: "€5.67",
      picture: "http://dummyimage.com/599x589.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 88,
      name: "Water, Tap",
      price: "€54.73",
      picture: "http://dummyimage.com/730x410.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 89,
      name: "Tea - Camomele",
      price: "€26.80",
      picture: "http://dummyimage.com/536x719.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 90,
      name: "Pears - Bosc",
      price: "€97.87",
      picture: "http://dummyimage.com/697x782.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 91,
      name: "Cheese - Comte",
      price: "€52.45",
      picture: "http://dummyimage.com/574x630.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 92,
      name: "Langers - Ruby Red Grapfruit",
      price: "€28.97",
      picture: "http://dummyimage.com/668x639.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 93,
      name: "Tomato - Green",
      price: "€9.99",
      picture: "http://dummyimage.com/432x455.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 94,
      name: "Wine - Kwv Chenin Blanc South",
      price: "€81.30",
      picture: "http://dummyimage.com/710x581.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 95,
      name: "Pepper - Black, Ground",
      price: "€25.47",
      picture: "http://dummyimage.com/737x495.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 96,
      name: "Sprouts Dikon",
      price: "€42.19",
      picture: "http://dummyimage.com/739x537.png/ff4444/ffffff",
      category: "category1"
    },
    {
      id: 97,
      name: "Wine - White, Ej Gallo",
      price: "€0.18",
      picture: "http://dummyimage.com/678x779.png/dddddd/000000",
      category: "category2"
    },
    {
      id: 98,
      name: "Ginger - Crystalized",
      price: "€23.67",
      picture: "http://dummyimage.com/457x475.png/5fa2dd/ffffff",
      category: "category1"
    },
    {
      id: 99,
      name: "Nantucket - Orange Mango Cktl",
      price: "€79.38",
      picture: "http://dummyimage.com/695x746.png/cc0000/ffffff",
      category: "category1"
    },
    {
      id: 100,
      name: "Straw - Regular",
      price: "€92.58",
      picture: "http://dummyimage.com/564x568.png/cc0000/ffffff",
      category: "category1"
    }
  ],

  users: [
    {
      id: 1,
      firstName: "Paul",
      lastName: "Buhai",
      email: "paul.buhai@eestec.ro",
      password: "test"
    },
    {
      id: 2,
      firstName: "Eduard",
      lastName: "Istrate",
      email: "eduard.istrate@eestec.ro",
      password: "test"
    },
    {
      id: 3,
      firstName: "Andrei",
      lastName: "Stanciu",
      email: "andrei.stanciu@eestec.ro",
      password: "test"
    }
  ],

  orders: [
    {
      id: 1,
      userID: 1,
      products: [
        {
          name: "",
          price: "€16.75"
        }
      ],
      total: "€10",
      date: "",
      status: ""
    }
  ]
};
